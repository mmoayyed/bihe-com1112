/*******************************************************************************
 * Name: Misagh Moayyed
 * Date: July 10th 2010
 * Assignment: A java application to read, parse and print a set of contacts from a file.
 ******************************************************************************/
package org.bihe.com1112;

import java.io.PrintStream;

public class Contact {
	private String firstName;
	private String lastName;
	private String email;

	private int age;
	private String company;
	private String notes;

	/**
	 * A contact object cannot be created unless at least 3 values are
	 * specified.
	 */
	public Contact(final String name, final String lastName, final String email) {
		setFirstName(name);
		setLastName(lastName);
		setEmail(email);
	}

	/**
	 * Remember: not overriding the equals() method is still considered a
	 * valid choice. Two objects can have the same hash code, but may not be
	 * equal to each other. BUT, if you override the equals() method you must
	 * make sure the hashCode is also overridden. If two objects are equal, they
	 * must have the same hash code.
	 */
	@Override
	public boolean equals(final Object obj) {
		boolean isEqual = false;
		if (obj != null && obj != this && obj instanceof Contact) {
			final Contact contact = (Contact) obj;
			isEqual = getEmail().equalsIgnoreCase(contact.getEmail());
		}
		return isEqual;

	}

	public int getAge() {
		return age;
	}

	public String getCompany() {
		return company;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getNotes() {
		return notes;
	}

	/**
	 * If two objects are considered equal, then their hashCode must be the
	 * same. Since we consider two contact objects to be equal if they have the
	 * same email address, we will calculate a hash code based on the contact
	 * email address
	 */
	@Override
	public int hashCode() {
		return getEmail().hashCode();
	}

	public void print(final PrintStream stream) {
		stream.print("First Name: ");
		stream.println(getFirstName());

		stream.print("Last Name: ");
		stream.println(getLastName());

		stream.print("Email Address: ");
		stream.println(getEmail());

		if (getAge() > 0) {
			stream.print("Age: ");
			stream.println(getAge());
		}

		if (getCompany() != null && getCompany().trim().length() > 0) {
			stream.print("Company: ");
			stream.println(getCompany());
		}

		if (getNotes() != null && getNotes().trim().length() > 0) {
			stream.print("Notes: ");
			stream.println(getNotes());
		}
		stream.println();
	}

	public void setAge(final int age) {
		if (age < 0)
			throw new InvalidContactException("Negative parameter value for age is specified: %d", age);
		this.age = age;
	}

	public void setCompany(final String company) {
		this.company = company;
	}

	/**
	 * According to the rules of the assignment, a valid email address is one
	 * that has the '@' in it.
	 */
	public void setEmail(final String email) {
		if (email == null || email.trim().length() == 0 || email.indexOf("@") == -1)
			throw new InvalidContactException(
					"Invalid parameter value for email without the character '@' is specified for email %s",
					email);
		this.email = email;
	}

	/**
	 * According to the rules of the assignment, a valid first/last name is
	 * required.
	 */
	public void setFirstName(final String firstName) {
		if (firstName == null || firstName.trim().length() == 0)
			throw new InvalidContactException(
					"Invalid blank parameter value for first name is specified for first name %s", firstName);

		this.firstName = firstName;
	}

	/**
	 * According to the rules of the assignment, a valid first/last name is
	 * required.
	 */
	public void setLastName(final String lastName) {
		if (lastName == null || lastName.trim().length() == 0)
			throw new InvalidContactException(
					"Invalid blank parameter value for last name is specified for last name %s", lastName);

		this.lastName = lastName;
	}

	public void setNotes(final String notes) {
		this.notes = notes;
	}
}
