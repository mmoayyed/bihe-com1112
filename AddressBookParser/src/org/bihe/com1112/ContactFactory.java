/*******************************************************************************
 * Name: Misagh Moayyed
 * Date: July 10th 2010
 * Assignment: A java application to read, parse and print a set of contacts from a file.
 ******************************************************************************/
package org.bihe.com1112;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.StringTokenizer;

public abstract class ContactFactory {

	/*
	 * Lets use a constant here. If the rules changed and wanted to separate the
	 * fields by $, all we have to do is change one value here.
	 */
	private static final String DELIMITER_CHARACTER = ",";

	/*
	 * Remember: left-hand side should always be an interface type. This is
	 * called Information Hiding.
	 * 
	 * This method is designed in such way that it returns set of contact
	 * objects from the file. contacts w/ duplicate email addresses are not
	 * added to the set. If there are any other problems with the file, its
	 * path, format, and data the process will be stopped. So, for this method
	 * to work correctly, correct file data must exist.
	 */
	public static Set<Contact> getContacts(final File contactFile) {

		if (!contactFile.exists())
			throw new InvalidContactException("Contact file %s does not exist at the specified path",
					contactFile.getPath());

		if (!contactFile.canRead())
			throw new InvalidContactException("Contact file %s can not be read at the specified path",
					contactFile.getPath());

		if (contactFile.length() == 0)
			throw new InvalidContactException("Contact file %s is empty at the specified path", contactFile
					.getPath());

		/*
		 * We want keep contacts inside a set exactly how they were added in the
		 * file. So we use a LinkedHashSet to keep the order. Just as well, you
		 * could use any set implementation. You should note that whoever calls
		 * this method, has no idea what type of set we use...and why should
		 * they ?! All they know is that a Set is returned.
		 * 
		 * Remember: left-hand side is always an interface...
		 */
		Set<Contact> contactsSet = null;
		Reader reader = null;
		BufferedReader buffer = null;
		String inputLine = null;

		try {
			reader = new FileReader(contactFile);
			buffer = new BufferedReader(reader);

			contactsSet = new LinkedHashSet<Contact>();

			while ((inputLine = buffer.readLine()) != null)
				/*
				 * This is not needed, but just to be nice, we may ignore blank
				 * lines.
				 */
				if (inputLine.trim().length() > 0) {

					/*
					 * By using a tokenizer, we are also assuming that at least
					 * there's one character between each delimiter character.
					 * For example like this:
					 * 
					 * Name,LastName,Email,12, ,MyNotes
					 * 
					 * IMPORTANT: You can see above that company is left blank
					 * with a space just before the notes. This extra space for
					 * optional fields is required for tokenizer, otherwise, it
					 * will not see the company field and think that MyNotes is
					 * actually the company!
					 * 
					 * Another possible idea would be to use split() or even
					 * regular expressions.
					 */
					final StringTokenizer tokenizer = new StringTokenizer(inputLine, DELIMITER_CHARACTER);

					if (tokenizer.countTokens() < 3 || tokenizer.countTokens() > 6)
						throw new InvalidContactException(
								"Invalid number of fields specified on input line: %s", inputLine);

					final Contact myContact = new Contact(tokenizer.nextToken().trim(), tokenizer.nextToken()
							.trim(), tokenizer.nextToken().trim());
					if (tokenizer.hasMoreTokens())
						myContact.setAge(Integer.parseInt(tokenizer.nextToken().trim()));

					if (tokenizer.hasMoreTokens())
						myContact.setCompany(tokenizer.nextToken().trim());

					if (tokenizer.hasMoreTokens())
						myContact.setNotes(tokenizer.nextToken().trim());

					/*
					 * The set will take care of the duplicate items since we
					 * have defined the rules inside the Contact class.
					 * 
					 * We dont care what kind of set it is. Who cares! As long
					 * as it's a set, it should follow the set rules: no
					 * duplicates are allowed.
					 */
					contactsSet.add(myContact);
				}
		} catch (final NumberFormatException e) {
			throw new InvalidContactException("Invalid value of age is specified for input line %s",
					inputLine);
		} catch (final IOException e) {
			throw new InvalidContactException("An IO error has occurred: %s", e, e.getMessage());
		} finally {
			try {
				/*
				 * Dont forget to close the resources!
				 */
				if (buffer != null)
					buffer.close();

				if (reader != null)
					reader.close();
			} catch (final IOException e) {
				throw new InvalidContactException("An IO error while closing the file has occurred: %s", e, e
						.getMessage());
			}
		}

		return contactsSet;
	}
}
