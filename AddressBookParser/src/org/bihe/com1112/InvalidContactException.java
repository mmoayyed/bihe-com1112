/*******************************************************************************
 * Name: Misagh Moayyed
 * Date: July 10th 2010
 * Assignment: A java application to read, parse and print a set of contacts from a file.
 ******************************************************************************/
package org.bihe.com1112;

public class InvalidContactException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public InvalidContactException(final String arg0, final Object... args) {
		this(arg0, null, args);
	}

	public InvalidContactException(final String arg0, final Throwable arg1, final Object... args) {
		super(String.format(arg0, args), arg1);
	}

}
