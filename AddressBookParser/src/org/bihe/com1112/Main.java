/*******************************************************************************
 * Name: Misagh Moayyed
 * Date: July 10th 2010
 * Assignment: A java application to read, parse and print a set of contacts from a file.
 ******************************************************************************/
package org.bihe.com1112;

import java.io.File;
import java.util.Set;

public class Main {

	public static void main(final String[] args) {
		try {
			if (args.length != 1)
				throw new InvalidContactException(
				        "Contact file must be specified as the first and only command-line argument");

			/*
			 * Grab the file name from the first command line argument
			 */
			final File contactFile = new File(args[0]);

			/*
			 * Parse all the contacts specified in the file and return the complete
			 * set of all contacts
			 */
			final Set<Contact> contacts = ContactFactory.getContacts(contactFile);

			/*
			 * Print contacts to the given stream.
			 */
			for (final Contact contact : contacts)
				contact.print(System.out);
		} catch (final InvalidContactException e) {
			System.err.println(e.getMessage());
		}
	}

}
