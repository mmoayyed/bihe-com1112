/*******************************************************************************
 * Name: Sahba Tashakori
 * Date: 05/04/2012
 * Description: This is a small shopping cart application that allows the user to order a
 *              number of fixed items from the command prompt, calculates the final
 *              price and returns the total based on the available credit line.
 ******************************************************************************/
package org.bihe.com1112.main;

import org.bihe.com1112.entity.Customer;
import org.bihe.com1112.utility.SavitchIn;
import org.bihe.com1112.utility.Transaction;


public class Main {

    public static void main(final String[] args) {
        boolean mustContinue = true;
        boolean newCustomer = true;
        Customer currentCustomer = null;

        while (mustContinue) {
            if (newCustomer) {
                System.out.print("Please enter the customer name: ");
                final String customerName = SavitchIn.readLineWord();
                currentCustomer = new Customer(customerName);
                System.out.println(currentCustomer);
            }
            System.out.print("Please enter the order: ");

            final String orders = SavitchIn.readLine().trim();
            if (orders == null || orders.length() == 0) {

                System.out.print("Invalid order! Do you want to proceed with the current customer (y/n)? ");
                final char choice = SavitchIn.readLineNonwhiteChar();
                newCustomer = !(choice == 'y' || choice == 'Y');

            } else {
                final Transaction transaction = new Transaction(currentCustomer, orders);
                if (transaction.isValidTransaction()) {
                    final char choice = transaction.finalizeTransaction();
                    switch (choice) {
                    case 'y':
                    case 'Y':
                        newCustomer = true;
                        break;
                    case 'q':
                    case 'Q':
                        mustContinue = false;
                        break;
                    case 'c':
                    case 'C':
                        newCustomer = false;
                        break;
                    default:
                        mustContinue = false;
                        break;
                    }

                } else {
                    System.out.println("The order format is not acceptable. Please try again.");
                    newCustomer = true;
                }

            }

        }
        System.out.println("The end.");
    }

}
