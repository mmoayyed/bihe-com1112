/**
 * Name: Sahba Tashakori
 * Date: June 4, 2012
 * Description: Shop assistance application
 */

package org.bihe.com1112.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bihe.com1112.entity.AbstractProduct;
import org.bihe.com1112.entity.Customer;
import org.bihe.com1112.entity.ProductFactory;
import org.bihe.com1112.exception.TransactionException;

public class Transaction {
    private final Customer        customer;

    private List<AbstractProduct> productList = null;

    private double                netCost;

    private final String          order;

    public Transaction(final Customer customer, final String order) {
        this.customer = customer;
        this.order = order;
        final String[]        orders = order.split(" ");

        if (orders.length < 2 || orders.length % 2 != 0)
            throw new TransactionException("Invalid transaction (inputs shorter than desired)");

        this.productList = new ArrayList<AbstractProduct>(orders.length);

        try {
            for (int i = 0, j = 0; j < orders.length / 2; i += 2, j++) {
                final int quantity = Integer.parseInt(orders[i + 1]);
                final AbstractProduct prod = ProductFactory.makeProduct(orders[i].charAt(0), quantity);
                getProductList().add(prod);
            }
        } catch (final NumberFormatException e) {
            throw new IllegalArgumentException("You should have enterted an invalid value for the total number of products", e);
        }
    }

    public final String getReceipt() {
        calculateNetCost();
        final StringBuilder messageBuilder = new StringBuilder();
        if (getCustomer().checkCrediblity(getNetCost())) {
            messageBuilder.append(getCustomer().getName());
            messageBuilder.append(" - (");
            messageBuilder.append(getOrder());
            messageBuilder.append(") - ");
            messageBuilder.append(getNetCost() * 10);
            messageBuilder.append("R");
            return messageBuilder.toString();
        } else
            throw new TransactionException("Credit Not enough! Total Cost: %.2f, Balance: %.2f", getNetCost(), getCustomer().getBalance());
    }

    public final Collection<AbstractProduct> getSortedProducts(final int flag) {
        final List<AbstractProduct> list = cloneProductList();
        Collections.sort(list, AbstractProduct.getComparator(flag));
        return list;
    }

    private void calculateNetCost() {
        setNetCost(0);

        for (int i = 0; i < getProductList().size(); i++)
            setNetCost(getNetCost() + getProductList().get(i).getNetPrice());

    }

    private List<AbstractProduct> cloneProductList() {
        final List<AbstractProduct> list = new ArrayList<AbstractProduct>(getProductList().size());
        for (final AbstractProduct abstractProduct : getProductList())
            list.add(abstractProduct.clone());
        return list;
    }

    private Customer getCustomer() {
        return this.customer;
    }

    private double getNetCost() {
        return this.netCost;
    }

    private String getOrder() {
        return this.order;
    }

    private List<AbstractProduct> getProductList() {
        return this.productList;
    }

    private void setNetCost(final double netCost) {
        this.netCost = netCost;
    }
}
