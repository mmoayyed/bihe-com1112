/**
 * Name: Sahba Tashakori
 * Date: June 4, 2012
 * Description: Shop assistance application
 */

package org.bihe.com1112.main;

import java.util.Collection;

import org.bihe.com1112.entity.AbstractProduct;
import org.bihe.com1112.entity.Customer;
import org.bihe.com1112.exception.TransactionException;
import org.bihe.com1112.util.SavitchIn;
import org.bihe.com1112.util.Transaction;

public class Main {

    private static boolean mustContinue = true;
    private static boolean newCustomer  = true;

    public static void main(final String[] args) {

        Customer currentCustomer = null;
        while (mustContinue) {
            if (newCustomer) {
                System.out.print("Please enter the customer name:");
                final String customerName = SavitchIn.readWord();
                currentCustomer = new Customer(customerName);
                System.out.println(currentCustomer.toString());
            }
            System.out.println("Please enter the order:");
            final String orders = SavitchIn.readLine().trim();
            if (orders == null || orders.length() == 0) {
                System.out.print("Invalid order!");
                Main.shouldContinue(false);
            } else
                try {
                    final Transaction transaction = new Transaction(currentCustomer, orders);

                    System.out.println(transaction.getReceipt());
                    System.out.println("Please choose the sort Option [Serial (0), Name/Serial(1)]:");
                    final int sortChoice = SavitchIn.readLineInt();

                    final Collection<AbstractProduct> col = transaction.getSortedProducts(sortChoice);

                    for (final AbstractProduct p : col)
                        System.out.println(p.toString());
                    Main.shouldContinue(true);

                } catch (final TransactionException ex) {
                    System.out.println(ex.getMessage());
                    Main.shouldContinue(false);
                } catch (final IllegalArgumentException ex) {
                    System.out.println("You have entered an invalid option");
                    Main.shouldContinue(true);
                }
        }
        System.out.println("The end!");
    }

    private static void shouldContinue(final boolean isExitStatement) {
        if (!isExitStatement) {
            System.out.println("Do you want to enter more orders [y/n]?");
            final char choice = SavitchIn.readLineNonwhiteChar();
            if (choice == 'y' || choice == 'Y') {
                newCustomer = false;
                mustContinue = true;
            } else
                Main.shouldContinue(true);
        } else {
            System.out.print("Do you want to exit [y/n]?");
            final char exit = SavitchIn.readLineNonwhiteChar();
            if (exit == 'y' || exit == 'Y')
                mustContinue = false;
            else {
                mustContinue = true;
                newCustomer = true;
            }
        }
    }
}
