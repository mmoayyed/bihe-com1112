/**
 * Name: Sahba Tashakori
 * Date: June 4, 2012
 * Description: Shop assistance application
 */

package org.bihe.com1112.exception;

public class TransactionException extends RuntimeException {

    private static final long serialVersionUID = -6460194169566195597L;

    public TransactionException() {
        super();

    }

    public TransactionException(final String message) {
        super(message);

    }

    public TransactionException(final String arg0, final Object... args) {
        this(arg0, null, args);
    }

    public TransactionException(final String arg0, final Throwable arg1, final Object... args) {
        super(String.format(arg0, args), arg1);
    }
}
