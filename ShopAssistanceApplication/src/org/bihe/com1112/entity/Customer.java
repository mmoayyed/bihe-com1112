/**
 * Name: Sahba Tashakori
 * Date: June 4, 2012
 * Description: Shop assistance application
 */
package org.bihe.com1112.entity;

import java.util.Random;

public class Customer {

    private final double balance;
    private final String name;

    public Customer(final String name) {
        this.balance = new Random().nextDouble() * 14000 + 1000;
        this.name = name;
    }

    public boolean checkCrediblity(final double cost) {
        return this.balance >= cost;
    }

    public double getBalance() {
        return this.balance;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return "Name: " + getName() + ", " + String.format("Balance: %.2f", getBalance());
    }
}
