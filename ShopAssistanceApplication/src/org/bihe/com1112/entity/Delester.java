/**
 * Name: Sahba Tashakori
 * Date: June 4, 2012
 * Description: Shop assistance application
 */
package org.bihe.com1112.entity;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Delester extends AbstractProduct {
    private final static GregorianCalendar EXPIRATION_DATE = new GregorianCalendar();
    private final static int               VALIDITY        = 6;

    public Delester(final int delesterPrice, final int quantity, final String productSerial, final String productType) {

        super(delesterPrice, quantity, productSerial, productType);

        // Let's give the Delester 6 months interval
        int candidateDate;
        candidateDate = EXPIRATION_DATE.get(Calendar.MONTH) + VALIDITY;
        if (candidateDate > 11) {
            EXPIRATION_DATE.set(Calendar.YEAR, EXPIRATION_DATE.get(Calendar.YEAR) + 1);
            candidateDate -= 11;
        }

        EXPIRATION_DATE.set(Calendar.MONTH, candidateDate);
    }
}
