/**
 * Name: Sahba Tashakori
 * Date: June 4, 2012
 * Description: Shop assistance application
 */
package org.bihe.com1112.entity;

public final class ProductFactory {

    private static final int MILK_PRICE     = 750;
    private static final int CAN_PRICE      = 2500;
    private static final int DELESTER_PRICE = 800;


    public final static AbstractProduct makeProduct(final char code, final int quantity) {
        AbstractProduct product = null;

        switch (code) {
        case 'm':
        case 'M':
            product = new Milk(MILK_PRICE, quantity, ProductFactory.generateProductSerial(code), "Milk");
            break;
        case 'D':
        case 'd':
            product = new Delester(DELESTER_PRICE, quantity, ProductFactory.generateProductSerial(code), "Delester");
            break;
        case 'C':
        case 'c':
            product = new Can(CAN_PRICE, quantity, ProductFactory.generateProductSerial(code), "Can");
            break;
        default:
            throw new IllegalArgumentException("Invalid product code: " + code);
        }
        return product;
    }

    private static String generateProductSerial(final char productChar) {
        final StringBuilder serialBuilder = new StringBuilder();
        serialBuilder.append(productChar);
        serialBuilder.append(String.format("%.0f", Math.random() * 9));
        serialBuilder.append(String.format("%.0f", Math.random() * 9));
        return serialBuilder.toString();
    }

    private ProductFactory() {}
}
