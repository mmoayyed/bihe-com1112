/**
 * Name: Sahba Tashakori
 * Date: June 4, 2012
 * Description: Shop assistance application
 */
package org.bihe.com1112.entity;

public class Milk extends AbstractProduct {

    public enum PacketSize {
        NORMAL("Normal"), BIG("Big"), SMALL("Small");

        private String size;

        private PacketSize(final String size) {
            this.size = size;
        }

        public String getPacketSize() {
            return this.size;
        }
    }

    private final static PacketSize[] AVALAIBLE_SIZES = PacketSize.values();
    private final PacketSize          size;

    public Milk(final int milkprice, final int quantity, final String serial, final String productType) {
        super(milkprice, quantity, serial, productType);
        this.size = AVALAIBLE_SIZES[(int) Math.random() * AVALAIBLE_SIZES.length];

    }

    public PacketSize getSize() {
        return this.size;
    }

}
