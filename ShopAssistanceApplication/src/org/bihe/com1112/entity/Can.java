/**
 * Name: Sahba Tashakori
 * Date: June 4, 2012
 * Description: Shop assistance application
 */

package org.bihe.com1112.entity;

public class Can extends AbstractProduct {
	private final String manufacturer;
	private static final String[] AVAILABLE_COMPANYIES = { "CompanyA",
			"CompanyB", "CompanyC", "CompanyD" };

	public Can(int canPrice, int quantity, String ProductSerial,
			String productType) {
		super(canPrice, quantity, ProductSerial, productType);
		manufacturer = AVAILABLE_COMPANYIES[(int) (Math.random() * 4)];
	}

	public String getManufacturer() {
		return manufacturer;
	}

}
