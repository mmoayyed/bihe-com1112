/**
 * Name: Sahba Tashakori
 * Date: June 4, 2012
 * Description: Shop assistance application
 */

package org.bihe.com1112.entity;

import java.util.Comparator;

public abstract class AbstractProduct implements Comparable<AbstractProduct>, Cloneable {
    private static class AbstractProductNameComparator implements Comparator<AbstractProduct> {
        @Override
        public int compare(final AbstractProduct o1, final AbstractProduct o2) {
            return o1.compareTo(o2);
        }

    }

    private static class AbstractProductSerialComparator implements Comparator<AbstractProduct> {
        @Override
        public int compare(final AbstractProduct o1, final AbstractProduct o2) {
            return o1.getSerial().compareTo(o2.getSerial());
        }
    }

    public static final Comparator<AbstractProduct> getComparator(final int flag) {
        Comparator<AbstractProduct> comp = null;

        switch (flag) {
        case 1:
            comp = new AbstractProductNameComparator();
            break;
        default:
            comp = new AbstractProductSerialComparator();
            break;
        }
        return comp;
    }
    private final String serial;
    private final int    productPrice;

    private final int    quantity;

    private final String productType;

    public AbstractProduct(final int price, final int quantity, final String serial, final String ProductType) {
        this.quantity = Math.abs(quantity);
        this.productPrice = price;
        this.serial = serial;
        this.productType = ProductType;
    }

    public double getNetPrice() {
        double netPrice;
        if (this.productPrice != 0)
            netPrice = this.quantity * this.productPrice;
        else
            netPrice = 0;
        return netPrice;
    }

    @Override
    public AbstractProduct clone() {
        try {
            return (AbstractProduct) super.clone();
        } catch (final CloneNotSupportedException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public int compareTo(final AbstractProduct obj) {
        int result = getProductType().compareTo(obj.getProductType());
        if (result == 0)
            result = getSerial().compareTo(obj.getSerial());
        return result;
    }

    public int getProductPrice() {
        return this.productPrice;
    }

    public String getProductType() {
        return this.productType;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public String getSerial() {
        return this.serial;
    }

    @Override
    public String toString() {
        return "Product type: " + this.productType + ", Serial: " + this.serial;
    }

}
